from collections.abc import Sequence
from datetime import datetime
from typing import Literal, cast

import pandas as pd
from pyspark.sql import DataFrame, SparkSession


def get_table_as_df(spark: SparkSession, table: str) -> DataFrame:
    "Utility function for easy table retrieval as a dataframe."
    query = f"SELECT * FROM {table}"
    return spark.sql(query)


def python_to_sql_list(python_list: Sequence[int] | Sequence[str]) -> str:
    """Convert a non-empty `list` into a valid SQL list that can be used in
    queries.

    Examples:
    [] => '()'
    ['enwiki'] => '("enwiki")'
    ['enwiki',] => '("enwiki")'
    ['enwiki', "uzwiki"] => '("enwiki", "uzwiki")'
    [2020, 2021] => '(2020, 2021)'
    """
    if not python_list:
        return "()"
    if isinstance(python_list[0], int):
        return "(" + ", ".join([str(x) for x in python_list]) + ")"

    # Mypy currently doesn't support type refinements
    # for sequences via index expressions hence the cast.
    # see https://github.com/python/mypy/issues/9362
    # Can also use custom typeguards here to avoid the cast.
    python_list = cast(Sequence[str], python_list)
    return '("' + '", "'.join(python_list) + '")'


def get_time_buckets(
    start_date: datetime,
    end_date: datetime,
    frequency: Literal["yearly", "monthly"] = "monthly",
) -> list[str]:
    """Return a list of buckets from `start_date` through `end_date`
    fixed by `frequency`.
    """
    if frequency == "yearly":
        return [b.strftime("%Y") for b in pd.date_range(start_date, end_date, freq="Y")]
    elif frequency == "monthly":
        return [
            b.strftime("%Y-%m") for b in pd.date_range(start_date, end_date, freq="M")
        ]
