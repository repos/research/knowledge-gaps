import argparse
import logging
from collections.abc import Iterable
from datetime import datetime, timedelta
from typing import Literal

import pyspark.sql.functions as F
from pyspark.sql import DataFrame, SparkSession


def aggregate_pageview_hourly(
    pageview_hourly_df: DataFrame,
    date_cols: Iterable[Literal["year", "month", "day"]] = ["year", "month", "day"],
) -> DataFrame:
    """
    Returns
    -------
    Pageview data with a time_bucket date column, derived from the partition
    """

    @F.udf(returnType="string")
    def wiki_db(project: str) -> str:
        return f"{project.split('.')[0]}wiki"

    return (
        pageview_hourly_df.withColumn("wiki_db", wiki_db("project"))
        .withColumn("time_bucket", F.concat_ws("-", *date_cols).cast("date"))
        .groupby("wiki_db", "page_id", "time_bucket")
        .agg(F.sum("view_count").alias("pageviews"))
    )


def save_pageviews(pageviews_df: DataFrame, table: str, write_mode: str) -> None:
    """
    Parameters
    ----------
    pageviews_df:
        pageviews dataframe with a time_bucket date column
    table:
        hive table to write to
    write_mode:
        write mode of the hive table
    """
    # monthly time partitions with daily time_buckets, this will
    # result in 10 files of ~280mb
    time_partition_format = "yyyy-MM"
    partitions_per_time_partition = 10

    (
        pageviews_df.withColumn(
            "time_partition", F.date_format("time_bucket", time_partition_format)
        )
        .repartition(partitions_per_time_partition)
        .write.mode(write_mode)
        .format("hive")
        .partitionBy("time_partition")
        .saveAsTable(table)
    )


def pageviews_query(start_date: datetime, end_date: datetime) -> str:
    """
    Returns a hive query for hourly pageviews data.

    Note that the query WHERE clause filtering BETWEEN start/end does make
    use of the hive partitioning for the hourly pageview dataset (which is
    year/month/day/hour).
    """
    return f"""
        SELECT
            *
        FROM wmf.pageview_hourly
        WHERE
            substr(project, -10)=".wikipedia"
            AND agent_type = 'user'
            AND namespace_id=0
            AND TO_DATE(CAST(UNIX_TIMESTAMP(concat(
                CAST(year AS VARCHAR(4)),'-',
                CAST(month AS VARCHAR(2)),'-',
                CAST(day AS VARCHAR(2))
                ), 'yyyy-MM-dd') AS TIMESTAMP))
                BETWEEN DATE '{start_date.strftime("%Y-%m-%d")}'
                    AND DATE '{end_date.strftime("%Y-%m-%d")}'
    """


def make_yearly_ranges(
    start_date: datetime, end_date: datetime
) -> list[tuple[datetime, datetime]]:
    """
    Returns
    -------
    list of datetime tuples that splits the [start_date, end_date] into yearly batches
    """

    assert (
        start_date < end_date
    ), f"start_date ({start_date}) has to be before end_date ({end_date})"
    ranges = []
    while start_date <= end_date:
        year_end = datetime(start_date.year, 12, 31)
        ranges.append((start_date, min(end_date, year_end)))
        start_date = year_end + timedelta(days=1)
    return ranges


def execute_batched_pageviews(
    spark: SparkSession,
    start_date: datetime,
    end_date: datetime,
    table: str,
    overwrite: bool = False,
) -> None:
    """
    Execute a batch of spark jobs to generate an aggregated pageviews dataset.

    Calling this method will a sequence of spark jobs, each aggregataing the hourly
    pageviews data for a given year into monthly aggregates. The first batch job will
    create the `table`, while subsequent batches will append to it.

    Parameters
    ----------
    spark:
        spark session to use
    start_date:
        start datetime
    end_date:
        end datetime
    table:
        hive table to use
    overwrite:
        whether to overwrite the table for the initial batch
    """

    yearly_ranges = make_yearly_ranges(start_date, end_date)
    for i, (start, end) in enumerate(yearly_ranges):
        aggregated_df = aggregate_pageview_hourly(
            spark.sql(pageviews_query(start, end))
        )
        logging.info(f"batch job {i} writing to {table}")
        if i == 0:
            logging.info(f"first job creates table {table}")
            mode = "overwrite" if overwrite else "error"
            save_pageviews(aggregated_df, table, mode)
        else:
            save_pageviews(aggregated_df, table, "append")


def date_range_to_update(spark: SparkSession, table: str) -> tuple[datetime, datetime]:
    """
    Given an existing pageviews daily table, returns a tuple of datetime
    objects (start_date, end_date). a start date derived from the last
    available time_bucket in the existing table, to a end date for which
    we can assume that the hourly pageview data exists already.

    Parameters
    ----------
    spark:
        spark session to use
    table:
        hive table containing the daily pageviews to update

    Returns
    -------
    a tuple of (start_date, end_date)
    """

    partition_value = F.udf(lambda p: p[len("time_partition=") :], "string")
    time_partitions = [
        row["time_partition"]
        for row in (
            spark.sql(f"SHOW PARTITIONS {table}")
            .select(partition_value("partition").alias("time_partition"))
            .collect()
        )
    ]

    last_time_partition = max(time_partitions)

    last_time_bucket = next(
        row["last_time_bucket"]
        for row in (
            spark.sql(
                f"""
        SELECT MAX(time_bucket) as last_time_bucket  FROM {table}
        WHERE time_partition='{last_time_partition}'
    """
            ).collect()
        )
    )
    last_time_bucket = datetime.combine(last_time_bucket, datetime.min.time())
    start_date = last_time_bucket + timedelta(days=1)

    # choice of 2 days is somewhat arbitrary, i.e. we will verify with data
    # eng if the hourly pageviews ever get delayed. This will be more of
    # a concern when the knowledge gap pipeline becomes an incremental
    # pipeline instead of snapshot based.
    end_date = datetime.now() - timedelta(days=2)

    return start_date, end_date


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mode",
        choices=["backfill", "update", "range"],
        required=True,
        help="""backfill: aggregates the whole pageview hourly dataset in batches.
update: aggregate the new pageviews hourly data for an existing output table
range: aggregate pageviews hourly data for a date range
""",
    )
    parser.add_argument(
        "--start_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "If mode is 'range', a start_date needs to be specified "
            "in the following format: YYYYMMDD, e.g. 20220131."
        ),
    )
    parser.add_argument(
        "--end_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "If mode is 'range', an end_date needs to be specified "
            "in the following format: YYYYMMDD, e.g. 20220131."
        ),
    )
    parser.add_argument("--database_name", help="hive database to write to")
    parser.add_argument(
        "--table_prefix", default="", help="Optional prefix for hive table"
    )
    parser.add_argument(
        "--overwrite",
        default=False,
        help=(
            "Whether to overwrite the table for the initial "
            "batch of `backfill` or `range` mode"
        ),
    )

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    output_table = f"{args.database_name}.{args.table_prefix}pageviews_daily"

    if args.mode == "range":
        assert args.start_date and args.end_date, "start end end date must be specified"
        execute_batched_pageviews(
            spark, args.start_date, args.end_date, output_table, args.overwrite
        )
    elif args.mode == "backfill":
        start_date = datetime.strptime("20150501", "%Y%m%d")
        end_date = datetime.now() - timedelta(days=2)
        execute_batched_pageviews(
            spark, start_date, end_date, output_table, args.overwrite
        )
    elif args.mode == "update":
        # TODO we need to revisit/discuss this config for incremental datasets.
        # atm the end_date is 2 days before datetime.now() to account for some backlog
        # pageview hourly dataset
        start_date, end_date = date_range_to_update(spark, output_table)
        max_days = 365
        assert (
            end_date - start_date
        ).days < max_days, "aggregation period must be smaller than 365 days"
        ph_df = spark.sql(pageviews_query(start_date, end_date))
        pd_df = aggregate_pageview_hourly(ph_df)
        save_pageviews(pd_df, output_table, "append")


if __name__ == "__main__":
    main()
