from collections.abc import Sequence

import pyspark.sql.functions as F
from pyspark.sql import DataFrame, Row, SparkSession
from pyspark.sql.window import Window

from knowledge_gaps.util import python_to_sql_list


def wikipedia_pages_df(
    spark: SparkSession,
    mediawiki_snapshot: str,
    wikidata_snapshot: str,
    projects: Sequence[str] | None = None,
    page_table: str = "wmf_raw.mediawiki_page",
    page_link_table: str = "wmf.wikidata_item_page_link",
) -> DataFrame:
    """Create a DataFrame of Wikipedia articles, containing page ID,page
    title, correspond Wikidata item ID, Wikipedia project.

    Returns:
        root
        |-- wiki_db: string
        |-- page_id: long
        |-- page_title: string
        |-- qitem_id: string
    """
    projects_clause = (
        f"AND p.wiki_db in {python_to_sql_list(projects)}" if projects else ""
    )

    query = f"""
        SELECT DISTINCT p.wiki_db, p.page_id, p.page_title,
            wipl.item_id AS qitem_id
        FROM {page_table} p
        LEFT JOIN {page_link_table} wipl
        ON p.wiki_db = wipl.wiki_db
            AND p.page_id = wipl.page_id
        WHERE p.snapshot='{mediawiki_snapshot}'
            AND p.page_namespace=0
            AND p.page_is_redirect=0
            AND wipl.snapshot = '{wikidata_snapshot}'
            AND wipl.page_namespace = 0
            {projects_clause}
        """
    df = spark.sql(query).dropDuplicates()
    return df


def get_page_revision_counts(
    spark: SparkSession,
    time_bucket_format: str,
    mediawiki_snapshot: str,
    projects: list[str] | set[str] | None = None,
    revision_table: str = "wmf_raw.mediawiki_revision",
) -> DataFrame:
    """Get the number of revision counts aggregated into `time_buckets`
    for `projects`.

    Parameters
    ----------
    spark : SparkSession

    time_bucket_format: The time format to use for the time_bucket, e.g. 'yyyy-MM'

    mediawiki_snapshot: MediaWiki snapshot in the format 'YYYY-MM', e.g. '2022-01'.

    projects: List of Wikipedia project names in the format 'enwiki'. If not
      supplied, all projects are queried.

    revision_table: Revision table (namespaced with the database name) to query.

    Returns
    -------
        root
         |-- wiki_db: string (nullable = true)
         |-- page_id: long (nullable = true)
         |-- time_bucket: string (nullable = true)
         |-- page_revision_count: long (nullable = false)
    """
    out_df = (
        spark.table(revision_table)
        .where(f"snapshot='{mediawiki_snapshot}'")
        .where(F.length("rev_timestamp") > 8)  # noqa: PLR2004 (magic value comparison)
        .withColumn("ts", F.to_timestamp("rev_timestamp", "yyyyMMddHHmmss"))
        .where(F.col("ts").isNotNull())
        # .where(f"""ts BETWEEN '{start_date.strftime("%Y-%m-%d")}' AND
        # '{end_date.strftime("%Y-%m-%d")}'""")
        .withColumn("time_bucket", F.date_format("ts", time_bucket_format))
    )
    if projects:
        out_df = out_df.where(F.col("wiki_db").isin(projects))

    return out_df.groupby(
        "wiki_db", F.col("rev_page").alias("page_id"), "time_bucket"
    ).agg(F.count("rev_id").alias("page_revision_count"))


def get_article_created(
    spark: SparkSession,
    mediawiki_snapshot: str,
    time_bucket_format: str,
    projects: Sequence[str] | None = None,
    page_history_table: str = "wmf.mediawiki_page_history",
) -> DataFrame:
    """Return a dataframe with an `article_created` flag, which is 1
    for the time_bucket an article was created in.

    Note: the earliest creation timestamp is used - a page that has been
    deleted and created repeatedly does not count multiple times.

    Parameters
    ----------
    spark : SparkSession

    time_bucket_format: The time format to use for the time_bucket, e.g. 'yyyy-MM'

    mediawiki_snapshot: MediaWiki snapshot in the format 'yyyy-MM', e.g. '2022-01'.

    projects : List of Wikipedia project names in the format 'enwiki'. If not
      supplied, all projects are queried.

    page_history_table: Page history table (namespaced with the database name) to query.

    Returns
    -------
        root
         |-- wiki_db: string (nullable = true)
         |-- page_id: long (nullable = true)
         |-- time_bucket: string (nullable = true)
         |-- article_created: long (nullable = false)
    """

    projects_clause = (
        f"AND wiki_db in {python_to_sql_list(projects)}" if projects else ""
    )

    return (
        spark.table(page_history_table)
        .where(
            f"""
            snapshot='{mediawiki_snapshot}'
            AND page_id IS NOT NULL
            AND page_id!=0
            AND page_namespace=0
            AND page_is_redirect=0
            AND page_creation_timestamp IS NOT NULL
            {projects_clause}"""
        )
        .groupby("wiki_db", "page_id")
        .agg(F.min("page_creation_timestamp").alias("earliest_page_creation_timestamp"))
        .withColumn(
            "time_bucket",
            F.date_format("earliest_page_creation_timestamp", time_bucket_format),
        )
        .drop("earliest_page_creation_timestamp")
        .withColumn("article_created", F.lit(1))
    )


def append_illustrated_articles(
    spark: SparkSession,
    pages: DataFrame,
    mediawiki_snapshot: str,
    imagelinks_table: str,
    page_props_table: str,
) -> DataFrame:
    """Return a dataframe with an `multimedia` gap content features.

    Parameters
    ----------
    spark: SparkSession

    pages: Dataframe of pages for which to extract multimedia gap features

    mediawiki_snapshot: MediaWiki snapshot in the format 'YYYY-MM', e.g. '2022-01'.

    imagelinks_table: The hive table for image links

    page_props_table: The hive table for page props

    Returns
    -------
    The input pages dataframe with a multimedia column
    """

    @F.udf(returnType="double")
    def get_threshold(wiki_size: int) -> float:
        # change th to optimize precision vs recall. recommended val for accuracy = 5
        import math

        sze, th, lim = 50000, 15, 4
        if wiki_size >= sze:
            # if wiki_size > base size, scale threshold by (log of ws/bs) + 1
            return (math.log(wiki_size / sze, 10) + 1) * th
        # else scale th down by ratio bs/ws, w min possible val of th = th/limiting val
        return max((wiki_size / sze) * th, th / lim)

    # wiki_db, threshold, total
    project_properties = (
        pages.groupby("wiki_db")
        .agg(F.count("page_id").alias("total"))
        .withColumn("threshold", get_threshold("total"))
    )
    project_properties.createOrReplaceTempView("project_properties")

    threshold_window = Window.partitionBy("wiki_db", "il_to")

    allowed_images = (
        spark.sql(
            f"""SELECT mwil.wiki_db, il_to
        FROM {imagelinks_table} mwil
        JOIN project_properties
        ON mwil.wiki_db=project_properties.wiki_db
        WHERE il_from_namespace=0
        AND snapshot='{mediawiki_snapshot}'
        AND il_to not like '%\"%' AND il_to not like '%,%'"""
        )
        .withColumn("il_to_count", F.count("*").over(threshold_window))
        .where(F.col("il_to_count") > F.col("threshold"))
    )
    allowed_images.createOrReplaceTempView("allowed_images")

    image_pageids = spark.sql(
        f"""
        SELECT DISTINCT wiki_db, il1.il_from as pageid
        FROM {imagelinks_table} il1
        LEFT ANTI JOIN allowed_images
        ON allowed_images.wiki_db=il1.wiki_db AND allowed_images.il_to=il1.il_to
        WHERE il1.il_from_namespace=0
        AND il1.snapshot='{mediawiki_snapshot}'"""
    )

    image_pageids.createOrReplaceTempView("image_pageids")

    pageimage_pageids = spark.sql(
        f"""
        SELECT DISTINCT wiki_db, pp_page as pageid
        FROM {page_props_table} pp
        WHERE pp.snapshot='{mediawiki_snapshot}'
        AND pp_propname in ('page_image','page_image_free')"""
    )

    pageimage_pageids.createOrReplaceTempView("pageimage_pageids")

    all_image_pageids = spark.sql(
        """
        SELECT wiki_db, pageid, TRUE AS illustrated
        FROM image_pageids
        UNION
        SELECT wiki_db, pageid, TRUE AS illustrated
        FROM pageimage_pageids"""
    )

    all_image_pageids.createOrReplaceTempView("all_image_pageids")

    @F.udf(returnType="struct<illustrated:boolean>")
    def multimedia_feature(illustrated: bool) -> Row:
        if illustrated:
            return Row(illustrated=True)
        else:
            return Row(illustrated=False)

    appended = (
        pages.alias("p")
        .join(
            all_image_pageids.alias("a"),
            (F.col("p.wiki_db") == F.col("a.wiki_db"))
            & (F.col("p.page_id") == F.col("a.pageid")),
            "left",
        )
        .withColumn("multimedia", multimedia_feature("a.illustrated"))
        .select("p.*", "multimedia")
    )

    return appended
