-- Run:
-- hive --hivevar mediawiki_snapshot=2022-05 \
--      --hivevar wikidata_snapshot=2022-05-23 \
--      --hivevar wikis="'enwiki','uzwiki'" \
--      --hivevar database_name=knowledge_gaps \
--      --hivevar table_prefix=dev_ \
--      -f development.sql

-- Latest MW and Wikidata snapshots can be retrieved like so:
-- hadoop fs -ls -C /wmf/data/wmf/mediawiki/page_history | tail -n 1
-- hadoop fs -ls  -C /wmf/data/wmf/wikidata/item_page_link | tail -n 1

SET hive.mapred.mode = nonstrict;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.strict.checks.large.query = false;
SET mapreduce.map.memory.mb = 8192;
SET mapreduce.map.java.opts=-Xmx7372m;
SET mapreduce.reduce.memory.mb = 8192;
SET mapreduce.reduce.java.opts = -Xmx7372m;
SET mapreduce.job.reduces = 64;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_project_namespace_map;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_project_namespace_map AS
SELECT *
FROM wmf_raw.mediawiki_project_namespace_map
WHERE snapshot = "${mediawiki_snapshot}"
    AND namespace = 0
    AND dbname in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_project_namespace_map;

DROP TABLE IF EXISTS ${database_name}.${table_prefix}wikidata_item;
CREATE TABLE ${database_name}.${table_prefix}wikidata_item AS
SELECT DISTINCT(item_id) AS qitem_id
FROM wmf.wikidata_item_page_link
WHERE snapshot = "${wikidata_snapshot}"
    AND page_namespace = 0
    AND wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}wikidata_item;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}wikidata_entity;
CREATE TABLE ${database_name}.${table_prefix}wikidata_entity AS
SELECT we.*
FROM ${database_name}.${table_prefix}wikidata_item wi
INNER JOIN wmf.wikidata_entity we
ON wi.qitem_id = we.id
WHERE we.snapshot = "${wikidata_snapshot}"
    AND we.typ = 'item';
SELECT COUNT(1) FROM ${database_name}.${table_prefix}wikidata_entity;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}wikidata_entity_exploded;
CREATE TABLE ${database_name}.${table_prefix}wikidata_entity_exploded AS
SELECT id AS qitem_id,
       claim.mainSnak.property,
       claim.mainsnak.datavalue.value
FROM ${database_name}.${table_prefix}wikidata_entity
LATERAL VIEW explode(claims) claims AS claim;
SELECT COUNT(1) FROM ${database_name}.${table_prefix}wikidata_entity_exploded;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}content_gap_item;
CREATE TABLE ${database_name}.${table_prefix}content_gap_item AS
WITH
human AS (
    SELECT qitem_id, 1 AS is_human
    FROM ${database_name}.${table_prefix}wikidata_entity_exploded
    WHERE property = 'P31' AND GET_JSON_OBJECT(value, '$.id') = 'Q5'
),
gender AS (
    SELECT qitem_id, GET_JSON_OBJECT(value, '$.id') AS gender
    FROM ${database_name}.${table_prefix}wikidata_entity_exploded
    WHERE property = 'P21'
),
sexual_orientation AS (
    SELECT qitem_id, GET_JSON_OBJECT(value, '$.id') AS sexual_orientation
    FROM ${database_name}.${table_prefix}wikidata_entity_exploded
    WHERE property = 'P91'
)
SELECT wi.*
FROM ${database_name}.${table_prefix}wikidata_item wi
LEFT JOIN human h
    ON wi.qitem_id = h.qitem_id
LEFT JOIN gender g
    ON wi.qitem_id = g.qitem_id
LEFT JOIN sexual_orientation so
    ON wi.qitem_id = so.qitem_id
WHERE h.is_human IS NOT NULL
    AND g.gender IS NOT NULL
    AND so.sexual_orientation IS NOT NULL
LIMIT 100;

WITH location AS (
    SELECT qitem_id, 1 AS is_location
    FROM ${database_name}.${table_prefix}wikidata_entity_exploded
    WHERE property = 'P31'
        AND GET_JSON_OBJECT(value, '$.id') in ('Q6256', 'Q5107')
)
INSERT INTO ${database_name}.${table_prefix}content_gap_item
SELECT wi.*
FROM ${database_name}.${table_prefix}wikidata_item wi
LEFT JOIN location l
    ON wi.qitem_id = l.qitem_id
WHERE l.is_location IS NOT NULL
LIMIT 100;


WITH timeprop AS (
    SELECT qitem_id
    FROM ${database_name}.${table_prefix}wikidata_entity_exploded
    WHERE property in ('P569', 'P570', 'P580', 'P582', 'P577', 'P571',
                       'P1619', 'P576', 'P1191', 'P813', 'P1249', 'P575')
)
INSERT INTO ${database_name}.${table_prefix}content_gap_item
SELECT wi.*
FROM ${database_name}.${table_prefix}wikidata_item wi
LEFT JOIN timeprop t
    ON wi.qitem_id = t.qitem_id
LIMIT 100;
SELECT COUNT(1) FROM ${database_name}.${table_prefix}content_gap_item;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}wikidata_item_page_link;
CREATE TABLE ${database_name}.${table_prefix}wikidata_item_page_link AS
SELECT wipl.*
FROM wmf.wikidata_item_page_link wipl
INNER JOIN ${database_name}.${table_prefix}content_gap_item cgi
ON wipl.item_id = cgi.qitem_id
WHERE wipl.snapshot = "${wikidata_snapshot}"
    AND wipl.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}wikidata_item_page_link;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_page_history;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_page_history AS
SELECT mph.*
FROM wmf.mediawiki_page_history mph
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON mph.wiki_db = wipl.wiki_db
    AND mph.page_id = wipl.page_id
WHERE mph.snapshot = "${mediawiki_snapshot}"
    AND mph.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_page_history;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_revision;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_revision AS
SELECT mr.*
FROM wmf_raw.mediawiki_revision mr
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON mr.wiki_db = wipl.wiki_db
    AND mr.rev_page = wipl.page_id
WHERE mr.snapshot = "${mediawiki_snapshot}"
    AND mr.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_revision;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_page;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_page AS
SELECT mp.*
FROM wmf_raw.mediawiki_page mp
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON mp.wiki_db = wipl.wiki_db
    AND mp.page_id = wipl.page_id
WHERE mp.snapshot = "${mediawiki_snapshot}"
    AND mp.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_page;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}pageviews_daily;
CREATE TABLE ${database_name}.${table_prefix}pageviews_daily AS
SELECT pd.*
FROM knowledge_gaps.pageviews_daily pd
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON pd.page_id = wipl.page_id
WHERE pd.time_partition = '2021-12'
    AND pd.time_bucket = '2021-12-31'
    AND pd.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}pageviews_daily;

DROP TABLE IF EXISTS ${database_name}.${table_prefix}article_quality_scores;
CREATE TABLE ${database_name}.${table_prefix}article_quality_scores AS
SELECT aq.*
FROM article_quality.scores aq
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON aq.page_id = wipl.page_id AND aq.wiki_db = wipl.wiki_db
WHERE aq.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}article_quality_scores;

DROP TABLE IF EXISTS ${database_name}.${table_prefix}article_quality_features;
CREATE TABLE ${database_name}.${table_prefix}article_quality_features AS
SELECT aq.*
FROM article_quality.features aq
INNER JOIN ${database_name}.${table_prefix}wikidata_item_page_link wipl
ON aq.page_id = wipl.page_id AND aq.wiki_db = wipl.wiki_db
WHERE aq.wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}article_quality_features;

DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_imagelinks;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_imagelinks AS
SELECT *
FROM wmf_raw.mediawiki_imagelinks
WHERE snapshot = "${mediawiki_snapshot}"
    AND il_from_namespace = 0
    AND wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_imagelinks;


DROP TABLE IF EXISTS ${database_name}.${table_prefix}mediawiki_page_props;
CREATE TABLE ${database_name}.${table_prefix}mediawiki_page_props AS
SELECT *
FROM wmf_raw.mediawiki_page_props
WHERE snapshot = "${mediawiki_snapshot}"
    AND wiki_db in (${wikis});
SELECT COUNT(1) FROM ${database_name}.${table_prefix}mediawiki_page_props;
