import csv
import importlib.resources as importlib_resources
import logging
from collections.abc import ItemsView

# A WMF internal hive table for mapping between geographrical entitities.
wmf_geography_table = "canonical_data.countries"


def load_aggregation_logic() -> ItemsView[str, str]:
    """Mapping of QIDs -> regions not directly associated with them.

    e.g. Sahrawi Arab Democratic Republic (Q40362) -> Western Sahara (Q6250)

    :return:
        list of mapping q_id to q_id
    """
    pkg = importlib_resources.files("knowledge_gaps.config")
    pkg_data_file = pkg / "country_aggregation.tsv"
    tsvreader = csv.reader(pkg_data_file.open(encoding="utf-8"), delimiter="\t")

    expected_header = ["Aggregation", "From", "QID To", "QID From"]
    aggregation = {}
    assert next(tsvreader) == expected_header
    for line in tsvreader:
        try:
            qid_to = line[2]
            qid_from = line[3]
            aggregation[qid_from] = qid_to
        except Exception:
            logging.debug("Skipped: %s", line)
    return aggregation.items()
