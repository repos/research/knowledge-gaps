import csv
import importlib.resources as importlib_resources
from typing import TypeAlias

Properties: TypeAlias = list[tuple[str, str]]


def load_time_properties() -> Properties:
    """Load properties for time gap (e.g. P569: date of birth)"""
    return load_properties("time_properties.tsv")


def load_country_properties() -> Properties:
    """Load country properties for geography gap (e.g. P19: place of birth)"""
    return load_properties("country_properties.tsv")


def load_properties(properties_tsv: str) -> Properties:
    """Load properties data from file.

    :param properties_tsv:
        Name of file to load properties for.
    """

    pkg = importlib_resources.files("knowledge_gaps.config")
    pkg_data_file = pkg / properties_tsv
    tsvreader = csv.reader(pkg_data_file.open(encoding="utf-8"), delimiter="\t")

    expected_header = ["Property", "Label"]
    properties = []
    assert next(tsvreader) == expected_header
    for line in tsvreader:
        property = line[0]
        label = line[1]
        properties.append((property, label))
    return properties
