import csv
import importlib.resources as importlib_resources
from typing import TypeAlias

Categories: TypeAlias = list[tuple[str, str]]


def load_gender_gap_categories() -> Categories:
    """
    Load gender gap categories used for the content gap metrics. Allowed values
    are based on https://www.wikidata.org/wiki/Property_talk:P21.
    """
    return load_categories("gender_categories.tsv")


def load_sexual_orientation_gap_categories() -> Categories:
    """
    Sexual Orientation gap categories used for the content gap metrics. Allowed values
    are based on https://www.wikidata.org/wiki/Property_talk:P91.
    """
    return load_categories("sexual_orientation_categories.tsv")


def load_categories(categories_tsv: str) -> Categories:
    """Load content gap category to QID mapping from file.

    :param categories_tsv:
        Name of file to load categories for.
    """

    pkg = importlib_resources.files("knowledge_gaps.config")
    pkg_data_file = pkg / categories_tsv
    tsvreader = csv.reader(pkg_data_file.open(encoding="utf-8"), delimiter="\t")
    expected_header = ["Label", "QID"]
    categories = []

    assert next(tsvreader) == expected_header
    for line in tsvreader:
        label = line[0]
        qid = line[1]
        categories.append((qid, label))
    return categories
