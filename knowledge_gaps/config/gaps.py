"""
Definitions of top level content gaps
"""

gender = "gender"
sexual_orientation = "sexual_orientation"
geographic = "geographic"
time = "time"
multimedia = "multimedia"

all_gaps = [gender, sexual_orientation, geographic, time, multimedia]
