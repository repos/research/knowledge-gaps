"""
Configuration for the internal state of the knowledge gap pipeline
"""
from functools import partial


def make_hdfs_path(base: str, name: str) -> str:
    base = base if base[-1] != "/" else base[:-1]
    return f"{base}/{name}"


# path for generated datasets
content_gap_features = partial(make_hdfs_path, name="content_gap_features")
metric_features = partial(make_hdfs_path, name="metric_features")

metrics_by_category = partial(make_hdfs_path, name="metrics_by_category")
metrics_by_content_gap = partial(make_hdfs_path, name="metrics_by_content_gap")
metrics_by_category_all_wikis = partial(
    make_hdfs_path, name="metrics_by_category_all_wikis"
)
metrics_by_content_gap_all_wikis = partial(
    make_hdfs_path, name="metrics_by_content_gap_all_wikis"
)

metrics_normalized = partial(make_hdfs_path, name="metrics_normalized")

# metrics_by_category = partial(make_hdfs_path, name="metrics_by_category")
# metrics_totals = partial(make_hdfs_path, name="metrics_totals")
# metrics_global = partial(make_hdfs_path, name="metrics_global")
# metrics_normalized = partial(make_hdfs_path, name="metrics_normalized")
