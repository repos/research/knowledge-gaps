import argparse
from collections.abc import Iterable, Sequence
from datetime import datetime
from functools import reduce
from typing import Literal

import pyspark.sql.functions as F
from pyspark.sql import DataFrame, SparkSession, Window

from knowledge_gaps import page_history
from knowledge_gaps.config import files
from knowledge_gaps.util import get_time_buckets


def filter_non_canonical_wikis(
    canonical_wikis_df: DataFrame, feature_metric_df: DataFrame
) -> DataFrame:
    """
    Filter out non-canonical wikis, ie, any project
    that is not a wikimedia project, eg, wikiquotes
    """
    return feature_metric_df.join(
        F.broadcast(canonical_wikis_df),
        feature_metric_df["wiki_db"] == canonical_wikis_df["database_code"],
        "inner",
    ).drop("database_code")


def get_canonical_wikis(
    spark: SparkSession, canonical_wikis_table_name: str = "canonical_data.wikis"
) -> DataFrame:
    """
    Returns a list of canonical wikis, ie, only wikimedia wikis
    """
    return (
        spark.table(canonical_wikis_table_name)
        .select("database_code")
        .where(F.col("database_group").isin(["wikipedia", "commons"]))
    )


def get_quality_scores(
    spark: SparkSession,
    time_bucket_format: str,
    projects: list[str] | set[str] | None,
    article_quality_table: str,
) -> DataFrame:
    """Return article quality scores with the time bucket set to
    `mediawiki_snapshot` snapshot.

    Parameters
    ----------
    spark: Spark Session

    time_bucket_format:
        The time format to use for the time_bucket, e.g. 'yyyy-MM'

    projects:
        List of Wikipedia project names in the format 'enwiki'. If not
        supplied, all projects are queried.

    mediawiki_snapshot:
        MediaWiki snapshot in the format '2022-01'.

    article_quality_table:
        The database where the scores are saved.


    Returns
    -------
    root
     |-- wiki_db: string (nullable = true)
     |-- page_id: long (nullable = true)
     |-- time_bucket: string (nullable = false)
     |-- quality_score: float (nullable = false)

    """

    out_df = (
        spark.table(article_quality_table)
        .drop("item_id")
        .withColumn("time_bucket", F.date_format("revision_dt", time_bucket_format))
        .withColumnRenamed("wiki_id", "wiki_db")
    )
    if projects:
        out_df = out_df.where(F.col("wiki_db").isin(projects))

    # get the quality_score of the last revision in a given time bucket
    # TODO, once we use spark 3.3, remove hackery and instead use
    # .agg(F.max_by('quality_score', 'revision_dt').alias('quality_score'))
    @F.udf(returnType="string")
    def max_by_encode(ts: str, q: float) -> str:
        return f"{ts}|{q}"

    @F.udf(returnType="float")
    def max_by_decode(k: str) -> float:
        return float(k.split("|")[1])

    out_df = (
        out_df.withColumn(
            "encoded_la", max_by_encode("revision_dt", "language_agnostic_quality")
        )
        .withColumn("encoded_std", max_by_encode("revision_dt", "standard_quality"))
        .groupby("wiki_db", "page_id", "time_bucket")
        .agg(
            F.max("encoded_la").alias("encoded_la"),
            F.max("encoded_std").alias("encoded_std"),
        )
        .withColumn("quality_score", max_by_decode("encoded_la"))
        .withColumn("standard_quality", max_by_decode("encoded_std").cast("int"))
        .drop("encoded_la", "encoded_std")
    )
    return out_df


def get_pageviews(
    spark: SparkSession,
    time_bucket_format: str,
    projects: list[str] | set[str] | None,
    pageviews_table: str,
) -> DataFrame:
    """Return pageview counts aggregated for time buckets defined by
    `time_bucket_format`.

    Parameters
    ----------
    spark : Spark Session

    time_bucket_format:
        The time format to use for the time_bucket, e.g. 'yyyy-MM'

    projects:
        List of Wikipedia project names in the format 'enwiki'. If not
        supplied, all projects are queried.

    pageviews_table:
        The table where the daily pageviews are saved.


    Returns
    -------
    root
     |-- wiki_db: string (nullable = true)
     |-- page_id: long (nullable = true)
     |-- time_bucket: string (nullable = false)
     |-- pageviews: long (nullable = false)

    """

    pv_df = (
        spark.table(pageviews_table)
        .where(F.col("page_id").isNotNull())
        .where(F.col("page_id") != 0)
    )
    if projects:
        pv_df = pv_df.where(F.col("wiki_db").isin(projects))

    return (
        pv_df.withColumn(
            "time_bucket", F.date_format("time_bucket", time_bucket_format)
        )
        .groupby("wiki_db", "page_id", "time_bucket")
        .agg(F.sum("pageviews").alias("pageviews"))
    )


def get_all_timebuckets(
    article_created_df: DataFrame,
    time_buckets: Sequence[str],
) -> DataFrame:
    """Generate a dataframe that contains all possible time_buckets (starting
    with the time_bucket during which an article was created), for all pages
    and all wikis.
    """

    @F.udf(returnType="array<string>")
    def following_buckets(created_time_bucket: str) -> Sequence[str]:
        # if the article was created before the first time_bucket
        # all time_buckets are returned
        if created_time_bucket < time_buckets[0]:
            return time_buckets
        # if the article was created after the largest time_bucket,
        # the article cannot contribute to the statistics
        elif created_time_bucket > time_buckets[-1]:
            return []

        # return the time_buckets after and including the time_bucket
        # the article was created in
        i = time_buckets.index(created_time_bucket)
        return time_buckets[i:]

    return article_created_df.select(
        "wiki_db",
        "page_id",
        F.explode(following_buckets("time_bucket")).alias("time_bucket"),
    )


def join_feature_metrics(time_bucketed_dfs: Iterable[DataFrame]) -> DataFrame:
    """Return a combined dataframe by joining individual metrics dataframes.

    Parameters
    ----------
    time_bucketed_dfs : list[DataFrame]
        List of dataframes that must have unique cols [wiki_db, page_id, time_bucket]

    Returns
    -------
    DataFrame
        Full join of input dataframes on cols [wiki_db, page_id, time_bucket]

    """
    join_columns = ["wiki_db", "page_id", "time_bucket"]

    # safety check that each input df only contains a single row per join key columns
    def assert_distict_join_columns(df: DataFrame) -> None:
        counts = df.groupBy(join_columns).count().select("count").distinct().collect()
        assert (
            len(counts) == 1 and counts[0]["count"] == 1
        ), f"multiple rows per join columns for {df}"

    for input_df in time_bucketed_dfs:
        assert_distict_join_columns(input_df)

    return reduce(lambda x, y: x.join(y, join_columns, "full"), time_bucketed_dfs)


def get_feature_metrics(
    spark: SparkSession,
    start_date: datetime,
    end_date: datetime,
    time_bucket_freq: Literal["yearly", "monthly"],
    projects: list[str] | None,
    mediawiki_snapshot: str,
    mode: Literal["development", "production"],
    dev_database_name: str | None = None,
    dev_table_prefix: str | None = None,
) -> DataFrame:
    """Return a dataframe containing the following feature_metrics:
    page_revision_count, pageviews, and quality_score.

    Parameters
    ----------
    spark: Spark Session

    start_date:
        Start date for computing metrics.

    end_date:
        End date for computing metrics.

    time_bucket_freq:
        Time bucket frequency for aggregating metrics. Either 'monthly'
        or 'yearly'.

    projects:
        List of Wikipedia project names in the format 'enwiki'. If not
        supplied, all projects are queried.

    mediawiki_snapshot:
        MediaWiki snapshot in the format 'YYYY-MM', e.g. '2022-01'.

    wikidata_snapshot:
        Wikidata snapshot in the format 'YYYY-MM-DD', e.g. '2022-01-24'.

    mode:
        Either "production" or "development". Used to decide on the
        kinds of tables to retrieve data from. In "development" mode
        smaller tables are used for fast compute.

    dev_database_name:
        For development mode, the database used for retrieving data.

    dev_table_prefix:
        In development, table name prefix used for fetching data.

    Returns
    -------
        root
         |-- wiki_db: string (nullable = true)
         |-- page_id: long (nullable = true)
         |-- time_bucket: string (nullable = true)
         |-- article_created: long (nullable = false)
         |-- pageviews: long (nullable = true)
         |-- quality_score: float (nullable = true)
         |-- page_revision_count: long (nullable = true)

    """

    if mode == "development":
        article_quality_table = (
            f"{dev_database_name}.{dev_table_prefix}article_quality_scores"
        )
        pageviews_table = f"{dev_database_name}.{dev_table_prefix}pageviews_daily"
        revision_table = f"{dev_database_name}.{dev_table_prefix}mediawiki_revision"
        page_history_table = (
            f"{dev_database_name}.{dev_table_prefix}mediawiki_page_history"
        )
    else:
        article_quality_table = "research.article_quality_scores"
        pageviews_table = "knowledge_gaps.pageviews_daily"
        revision_table = "wmf_raw.mediawiki_revision"
        page_history_table = "wmf.mediawiki_page_history"

    time_bucket_formats = {"yearly": "yyyy", "monthly": "yyyy-MM"}
    time_bucket_format = time_bucket_formats[time_bucket_freq]

    article_created_df = page_history.get_article_created(
        spark, mediawiki_snapshot, time_bucket_format, projects, page_history_table
    )
    article_created_df = article_created_df.cache()

    pageviews_df = get_pageviews(spark, time_bucket_format, projects, pageviews_table)

    quality_scores_df = get_quality_scores(
        spark, time_bucket_format, projects, article_quality_table
    )

    revision_counts_df = page_history.get_page_revision_counts(
        spark, time_bucket_format, mediawiki_snapshot, projects, revision_table
    )

    canonical_wikis = get_canonical_wikis(spark=spark)

    feature_metric_dfs = [
        article_created_df,
        pageviews_df,
        quality_scores_df,
        revision_counts_df,
    ]

    filtered_feature_metric_dfs = [
        filter_non_canonical_wikis(
            canonical_wikis_df=canonical_wikis, feature_metric_df=df
        )
        for df in feature_metric_dfs
    ]
    # sparse metric features dataframe
    feature_metrics_df_sparse = join_feature_metrics(filtered_feature_metric_dfs)

    # a dataframe of all time_buckets to ensure there is a row for all
    time_buckets = get_time_buckets(start_date, end_date, time_bucket_freq)
    time_buckets_df = get_all_timebuckets(
        filter_non_canonical_wikis(
            canonical_wikis, feature_metric_df=article_created_df
        ),
        time_buckets,
    )

    # Ensure that
    # 1. There is a row for each (wiki,page,time_bucket),
    #   in case all feature metrics are missing for a given time_bucket
    # 2. filter time_buckets outside the provided start and end date, to avoid rows with
    #   ...partial metrics (e.g. with pageviews, but not revision counts)
    feature_metrics_df = feature_metrics_df_sparse.join(
        time_buckets_df,
        on=["wiki_db", "page_id", "time_bucket"],
        how="right",
    )

    # for the quality feature metrics, we want to forward fill missing values.
    forward_fill = (
        Window.partitionBy(["wiki_db", "page_id"])
        .orderBy("time_bucket")
        .rowsBetween(Window.unboundedPreceding, Window.currentRow)
    )
    feature_metrics_df_forward_filled = feature_metrics_df.withColumn(
        "standard_quality",
        F.last("standard_quality", ignorenulls=True).over(forward_fill),
    ).withColumn(
        "quality_score", F.last("quality_score", ignorenulls=True).over(forward_fill)
    )

    return feature_metrics_df_forward_filled


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "start_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "Compute metrics starting this date; in the "
            "following format: YYYYMMDD, e.g. 20220101."
        ),
    )
    parser.add_argument(
        "end_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "Compute metrics ending this date; in the "
            "following format: YYYYMMDD, e.g. 20220131."
        ),
    )
    parser.add_argument(
        "--time_bucket_freq",
        choices=["monthly", "yearly"],
        default="monthly",
        help="Frequency of time buckets for metrics aggregation.",
    )
    parser.add_argument(
        "--projects",
        type=lambda projects: projects.split(","),
        help="Comma separated list of projects, e.g. "
        '"cawiki,enwiki". If not supplied all projects '
        "will be used.",
    )
    parser.add_argument(
        "--mediawiki_snapshot", help="Mediawiki snapshot, e.g. 2022-05."
    )
    parser.add_argument(
        "--mode",
        choices=["production", "development"],
        default="production",
        help=(
            "Mode to run the script in. In production "
            "mode data will be generated using production "
            "tables, and in development mode smaller tables "
            "will be used to generate data."
        ),
    )

    parser.add_argument(
        "--dev_database_name",
        help="In development mode, this database used to retrieve smaller datasets.",
    )
    parser.add_argument(
        "--dev_table_prefix",
        default="",
        help="In development mode, the prefix can be used namespace tables.",
    )

    parser.add_argument(
        "--hdfs_dir",
        required=True,
        help="""The directory on hdfs that the pipeline uses to store
                        intermediate datasets""",
    )

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    feature_metrics_df = get_feature_metrics(
        spark,
        args.start_date,
        args.end_date,
        args.time_bucket_freq,
        args.projects,
        args.mediawiki_snapshot,
        args.mode,
        args.dev_database_name,
        args.dev_table_prefix,
    )

    # for production as of August 2022, this generates ~40GB of data,
    # let's aim for ~100MB files
    file_partitions = 400

    if args.mode == "development":
        file_partitions = 10
        feature_metrics_df = feature_metrics_df.cache()
        feature_metrics_df.show(n=50)

    (
        feature_metrics_df.repartition(file_partitions)
        .write.mode("overwrite")
        .format("parquet")
        .save(files.metric_features(args.hdfs_dir))
    )


if __name__ == "__main__":
    main()
