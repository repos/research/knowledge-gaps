import argparse
import logging
from collections.abc import Callable, Iterable
from datetime import datetime
from functools import reduce
from itertools import chain
from typing import Literal, TypeAlias

import pyspark.sql.functions as F
from pyspark.sql import Column, DataFrame, SparkSession

from knowledge_gaps.config import files, gaps

AggregationLevel: TypeAlias = Literal[
    # metrics for content gap categories (e.g. female category of the gender gap)
    "metrics_by_category",
    # metrics for content gaps (e.g. across all categories)
    "metrics_by_content_gap",
    # metrics across all wikis per category
    "metrics_by_category_all_wikis",
    # metrics across all wikis per content gap
    "metrics_by_content_gap_all_wikis",
]

# TODO: ideally, this should be a TypedDict so that totality
# is guaranteed. Though this would require a few (minor) changes to
# the code and we might need to cast when looping over values since
# mypy would no longer be able to tell that all of them are Dataframes.
AggregatedMetrics: TypeAlias = dict[AggregationLevel, DataFrame]

ContentGap: TypeAlias = Literal[
    "gender",
    "sexual_orientation",
    "geography_country",
    "geography_continent",
    "geography_wmf_region",
    "geography_cultural_wmf_region",
    "multimedia_illustrated",
    "time",
]


def aggregate_gender_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.where(F.col("is_human"))
        .withColumn("col", F.explode("gender"))
        .dropDuplicates()
        # for cases [[,]] -> null
        .withColumn(
            "col", F.when(F.col("gender.gender").isNull(), None).otherwise(F.col("col"))
        )
        .where(F.col("col").isNotNull())
        .where(F.col("col.gender").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col.gender")


def aggregate_sexual_orientation_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.where(F.col("is_human"))
        .withColumn("col", F.explode("sexual_orientation"))
        .dropDuplicates()
        # for cases [Qxxx,] -> null
        .withColumn(
            "col",
            F.when(F.col("col.sexual_orientation").isNull(), None).otherwise(
                F.col("col")
            ),
        )
        .where(F.col("col").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col.sexual_orientation")


def aggregate_cultural_wmf_region_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.withColumn("col", F.explode("geographic.cultural_model"))
        .dropDuplicates()
        .withColumn(
            "col",
            F.when(F.col("col.wikimedia_region") == "", None).otherwise(F.col("col")),
        )
        .where(F.col("col").isNotNull())
        .where(F.col("col.wikimedia_region").isNotNull())
    )

    return aggregate_knowledge_gap_metrics(exploded, "col.wikimedia_region")


def aggregate_country_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.withColumn("col", F.explode("geographic.geospatial_model"))
        .dropDuplicates()
        .withColumn(
            "col", F.when(F.col("col.geocode").isNull(), None).otherwise(F.col("col"))
        )
        .where(F.col("col").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col.geocode.country_code")


def aggregate_continent_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.withColumn("col", F.explode("geographic.geospatial_model"))
        .dropDuplicates()
        .withColumn(
            "col",
            F.when(F.col("col.un_continent") == "", None).otherwise(F.col("col")),
        )
        .where(F.col("col").isNotNull())
        .where(F.col("col.un_continent").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col.un_continent")


def aggregate_wmf_region_gap(df: DataFrame) -> AggregatedMetrics:
    exploded = (
        df.withColumn("col", F.explode("geographic.geospatial_model"))
        .dropDuplicates()
        .withColumn(
            "col",
            F.when(F.col("col.wikimedia_region") == "", None).otherwise(F.col("col")),
        )
        .where(F.col("col").isNotNull())
        .where(F.col("col.wikimedia_region").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col.wikimedia_region")


def aggregate_warped_time_gap(df: DataFrame) -> AggregatedMetrics:
    # TODO revisit how to categorize time.
    # a category per year does not scale, nor make sense
    # the approach below creates ~80 categories:
    # (10millenia,19centuries,9decades,40years,2 edge buckets)
    # inspired by Isaac's work on TREC:
    #  https://gitlab.wikimedia.org/isaacj/miscellaneous-wikimedia/-/blob/master/wikidata-properties-spark/article-age.ipynb
    #  https://public.paws.wmcloud.org/User:Isaac_(WMF)/TREC/TREC_2022_Data.ipynb
    @F.udf(returnType="string")
    def categorize_year(years: Iterable[int]) -> str:
        import numpy as np

        category = "Unknown"
        years = [y for y in years if y is not None]
        if years:
            year = int(np.median(years))
            # ruff: noqa: PLR2004  # allow magic value comparison for rest of the file
            if year < -10000:
                category = "10+ millenia BC"
            elif year < 0:
                category = f"{(-1*year) // 1000 + 1 }th millenia BC"
            elif year < 1900:
                category = f"{(year) // 100 + 1 }th century"
            elif year < 1990:
                category = f"{(year) // 10 }0s"
            elif year < 2030:
                category = f"year {year}"
            else:
                category = "year 2030+"

        return category

    exploded = (
        df.where(F.col("time.year").isNotNull())
        .withColumn("col", categorize_year("time.year"))
        .where(F.col("col").isNotNull())
    )
    return aggregate_knowledge_gap_metrics(exploded, "col")


def aggregate_illustrated_multimedia_gap(df: DataFrame) -> AggregatedMetrics:
    # TODO, a lot of unillustrated articles are
    # https://www.wikidata.org/wiki/Q4167410 are disambiguation
    # pages. Do we want to filter these?
    df = df.withColumn("illustrated", F.col("multimedia.illustrated").cast("string"))
    return aggregate_knowledge_gap_metrics(df, "illustrated")


def aggregate_knowledge_gap_metrics(
    df: DataFrame,
    category: str,
) -> AggregatedMetrics:
    """Aggregate content gap metrics for a dataframe with a column `category`
    The dataframe `df` has rows of timebucket,*content_features,*feature_metrics.
    """

    # the scalar metrics computed for each content gap
    scalar_aggregations = {
        "article_created": F.sum("article_created"),
        "pageviews_sum": F.sum("pageviews"),
        "pageviews_mean": F.mean("pageviews"),
        "standard_quality": F.mean("standard_quality"),
        "standard_quality_count": F.sum("standard_quality"),
        "quality_score": F.mean("quality_score"),
        "revision_count": F.sum("page_revision_count"),
    }
    # quantiles computed for each content gap
    # probabilities = [0.05, 0.25, 0.5, 0.75, 0.95]
    quantiles_probabilities = {
        "5th_percentile": 0.05,
        "25th_percentile": 0.25,
        "median": 0.5,
        "75th_percentile": 0.75,
        "95th_percentile": 0.95,
    }
    probabilities = list(quantiles_probabilities.values())

    quantiles_aggregations = {
        "article_created_quantiles": F.percentile_approx(
            "article_created", probabilities
        ),
        "pageviews_quantiles": F.percentile_approx("pageviews", probabilities),
        "quality_score_quantiles": F.percentile_approx("quality_score", probabilities),
        "revision_count_quantiles": F.percentile_approx(
            "page_revision_count", probabilities
        ),
    }

    def quantiles_struct(metric: str) -> Column:
        fields = [
            F.col(metric).getItem(i).alias(name)
            for (i, (name, _)) in enumerate(quantiles_probabilities.items())
        ]
        # for the aggregation all column names must be unique,
        # in the struct we can remove the quantiles qualifier
        metric = metric.removesuffix("_quantiles")
        return F.struct(*fields).alias(metric)

    quantiles_cols = [quantiles_struct(name) for name in quantiles_aggregations.keys()]

    # build set of aggregations to perform
    agg_ops = {}
    agg_ops.update(scalar_aggregations)
    agg_ops.update(quantiles_aggregations)

    agg_ops = {n: agg.alias(n) for n, agg in agg_ops.items()}

    # The metrics are aggregated separately for each aggregation level,
    # as nested aggregation would return incorrect results. E.g. a "sum"
    # could re-aggregated, but a "mean" cannot.
    group_by_cols: dict[AggregationLevel, list[str]] = {
        "metrics_by_category": [
            "wiki_db",
            "category",
            "time_bucket",
        ],
        "metrics_by_content_gap": ["wiki_db", "time_bucket"],
        "metrics_by_category_all_wikis": [
            "category",
            "time_bucket",
        ],
        "metrics_by_content_gap_all_wikis": ["time_bucket"],
    }

    df = df.withColumn("category", F.col(category))

    aggregated = {}
    for name, cols in group_by_cols.items():
        agg_df = df.groupBy(cols).agg(*agg_ops.values())
        aggregated[name] = agg_df.select(
            *cols,
            F.struct(*scalar_aggregations.keys()).alias("metrics"),
            F.struct(*quantiles_cols).alias("quantiles"),
        )

    return aggregated


def aggregate_all_content_gap_metrics(
    content_feature_df: DataFrame,
    feature_metrics_df: DataFrame,
    content_gaps: Iterable[ContentGap],
) -> AggregatedMetrics:
    """
    Computes content gap metrics

    Parameters
    ----------
    content_feature_df: content gap features per article dataframe
    feature_metrics_df: feature metrics per timebucket dataframe
    content_gaps: list of content gaps to compute metrics for
    """
    # named content gap aggregations functions
    # string names are used as to configure which content gaps to compute
    content_gaps_aggregations = {
        "gender": aggregate_gender_gap,
        "sexual_orientation": aggregate_sexual_orientation_gap,
        "geography_country": aggregate_country_gap,
        "geography_continent": aggregate_continent_gap,
        "geography_wmf_region": aggregate_wmf_region_gap,
        "geography_cultural_wmf_region": aggregate_cultural_wmf_region_gap,
        "time": aggregate_warped_time_gap,
        "multimedia_illustrated": aggregate_illustrated_multimedia_gap,
    }

    # determine which derived gaps to compute
    gaps_to_compute: dict[ContentGap, Callable[[DataFrame], AggregatedMetrics]] = {
        gap: content_gaps_aggregations[gap] for gap in content_gaps
    }

    # This join will explode the content features for every time_bucket
    # for feature metrics.
    df = content_feature_df.join(
        feature_metrics_df, ["wiki_db", "page_id"], "inner"
    ).cache()

    agg_df_all_gaps: dict[AggregationLevel, list[DataFrame]] = {}

    for gap_name, agg_fn in gaps_to_compute.items():
        # for each content gap, compute aggregates
        aggregations = agg_fn(df)
        for agg_name, agg_df in aggregations.items():
            # for each aggregate type, collect the dataframes for all content gaps
            content_agg_df = agg_df.withColumn("content_gap", F.lit(gap_name))
            dfs = agg_df_all_gaps.get(agg_name, [])
            dfs.append(content_agg_df)
            agg_df_all_gaps[agg_name] = dfs

    def union(dfs: Iterable[DataFrame]) -> DataFrame:
        return reduce(lambda x, y: x.union(y), dfs)

    return {name: union(dfs) for name, dfs in agg_df_all_gaps.items()}


def main() -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--hdfs_dir",
        required=True,
        help=(
            "The directory on hdfs that the pipeline uses to store "
            "intermediate datasets"
        ),
    )
    parser.add_argument(
        "--content_gaps",
        choices=gaps.all_gaps,
        nargs="+",
        default=gaps.all_gaps,
        help=(
            "Top level content gaps for which to generate metrics. "
            "e.g. gender geography"
        ),
    )
    parser.add_argument(
        "--sub_content_gaps",
        nargs="+",
        help=(
            "Sub content gaps for which to generate metrics. "
            "e.g. geography_country geography_wmf_region"
        ),
    )

    parser.add_argument(
        "--timeseries_type",
        choices=["sparse", "dense"],
        default="sparse",
        help=(
            "Applies to the normalized output "
            "sparse creates metrics as map<string,double> with a time_bucket as key. "
            "dense creates metrics as array<double>"
        ),
    )
    parser.add_argument(
        "--start_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "Applies to the normalized output. "
            "When --timeseries-type is 'dense', this argument is used "
            "to determine the start/end of the time_buckets array. "
            "Format: YYYYMMDD, e.g. 20220101."
        ),
    )
    parser.add_argument(
        "--end_date",
        type=lambda d: datetime.strptime(d, "%Y%m%d"),
        help=(
            "Applies to the normalized output. "
            "When --timeseries-type is 'dense', this argument is used "
            "to determine the start/end of the time_buckets array. "
            "Format: YYYYMMDD, e.g. 20220101."
        ),
    )
    parser.add_argument(
        "--time_bucket_freq",
        choices=["monthly", "yearly"],
        default="monthly",
        help=(
            "Applies to the normalized output. Frequency of "
            "time buckets for metrics timeseries"
        ),
    )

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    content_gap_feature_df = spark.read.parquet(
        files.content_gap_features(args.hdfs_dir)
    )
    feature_metrics_df = spark.read.parquet(files.metric_features(args.hdfs_dir))

    # the aggregated metrics data is not large, decrease the number of files written
    file_partitions = 20

    if args.sub_content_gaps:
        gaps_to_compute = args.sub_content_gaps
    else:
        # mapping of top level content gap category to specific content gap aggregations
        # TODO, using strings as config, as the aggregation function itself can only be
        # loaded on the worker once a spark context exists. improve this config in the
        #  absence of types / add tests
        sub_content_gaps: dict[str, list[ContentGap]] = {
            "gender": ["gender"],
            "sexual_orientation": ["sexual_orientation"],
            "geographic": [
                "geography_country",
                "geography_continent",
                "geography_wmf_region",
                "geography_cultural_wmf_region",
            ],
            "time": ["time"],
            "multimedia": ["multimedia_illustrated"],
        }
        # determine which derived gaps to compute
        gaps_to_compute = list(
            chain.from_iterable(
                [
                    sub_content_gaps[g]
                    for g in sub_content_gaps
                    if g in args.content_gaps
                ]
            )
        )

    # aggregating configured contents gaps for all aggregation levels
    for agg_name, agg_df in aggregate_all_content_gap_metrics(
        content_gap_feature_df, feature_metrics_df, gaps_to_compute
    ).items():
        path = files.make_hdfs_path(args.hdfs_dir, agg_name)
        logging.info(f"computing {agg_name}, writing to {path}")
        (
            agg_df.repartition(file_partitions)
            .write.mode("overwrite")
            .format("parquet")
            .save(path)
        )


if __name__ == "__main__":
    main()
