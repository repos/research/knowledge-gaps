import argparse
from collections.abc import Sequence
from typing import Literal

import pyspark.sql.functions as F
from pyspark.sql import DataFrame, SparkSession

from knowledge_gaps import page_history, wikidata
from knowledge_gaps.config import files, gap_categories, gap_properties, gaps, geography


def get_content_features(
    spark: SparkSession,
    mediawiki_snapshot: str,
    wikidata_snapshot: str,
    projects: list[str] | None,
    content_gap: Sequence[str],
    mode: Literal["development", "production"],
    dev_database_name: str | None = None,
    dev_table_prefix: str | None = None,
) -> DataFrame:
    if mode == "development":
        mw_project_namespace_map_table = (
            f"{dev_database_name}.{dev_table_prefix}mediawiki_project_namespace_map"
        )
        wd_item_page_link_table = (
            f"{dev_database_name}.{dev_table_prefix}wikidata_item_page_link"
        )
        wd_entity_table = f"{dev_database_name}.{dev_table_prefix}wikidata_entity"
        page_table = f"{dev_database_name}.{dev_table_prefix}mediawiki_page"
        imagelinks_table = f"{dev_database_name}.{dev_table_prefix}mediawiki_imagelinks"
        page_props_table = f"{dev_database_name}.{dev_table_prefix}mediawiki_page_props"

    else:
        mw_project_namespace_map_table = "wmf_raw.mediawiki_project_namespace_map"
        wd_item_page_link_table = "wmf.wikidata_item_page_link"
        wd_entity_table = "wmf.wikidata_entity"
        page_table = "wmf_raw.mediawiki_page"
        imagelinks_table = "wmf_raw.mediawiki_imagelinks"
        page_props_table = "wmf_raw.mediawiki_page_props"

    qids = wikidata.wikidata_relevant_qitems_df(
        spark,
        wikidata_snapshot,
        mediawiki_snapshot,
        projects,
        mw_project_namespace_map_table,
        wd_item_page_link_table,
    ).cache()

    prop = wikidata.wikidata_relevant_properties(
        spark, wikidata_snapshot, qids, wd_entity_table=wd_entity_table
    ).cache()

    pages = page_history.wikipedia_pages_df(
        spark,
        mediawiki_snapshot,
        wikidata_snapshot,
        projects,
        page_table,
        wd_item_page_link_table,
    ).cache()

    qids = wikidata.append_is_human(qids, prop)

    if gaps.gender in content_gap:
        gender_cat = gap_categories.load_gender_gap_categories()
        qid_to_gender = spark.createDataFrame(gender_cat, ["qid", "gender"])
        qids = wikidata.append_gender_feature(qids, prop, qid_to_gender)

    if gaps.sexual_orientation in content_gap:
        sexual_orientation_cat = gap_categories.load_sexual_orientation_gap_categories()
        qid_to_label = spark.createDataFrame(
            sexual_orientation_cat, ["qid", "sexual_orientation"]
        )
        qids = wikidata.append_sexual_orientation_feature(qids, prop, qid_to_label)

    if gaps.geographic in content_gap:
        # wmf based resource of geographical data
        geography_mappings = spark.table(geography.wmf_geography_table)
        territories_aggregation = spark.createDataFrame(
            geography.load_aggregation_logic(), ["from_qid", "to_qid"]
        )
        region_prop = gap_properties.load_country_properties()
        region_prop_list = [p[0] for p in region_prop]
        qids = wikidata.append_geographic_feature(
            qids, prop, geography_mappings, territories_aggregation, region_prop_list
        )

    if gaps.time in content_gap:
        time_prop = gap_properties.load_time_properties()
        time_prop_list = [p[0] for p in time_prop]
        qids = wikidata.append_time_feature(qids, prop, time_prop_list)

    if gaps.multimedia in content_gap:
        # the multimedia gap content feature is not based on wikidata,
        # and is thus added as a column to the pages dataframe.
        # the tables mediawiki_imagelinks and the mediawiki_page_props
        # are used to determine wheter articles are illustrated.
        pages = page_history.append_illustrated_articles(
            spark, pages, mediawiki_snapshot, imagelinks_table, page_props_table
        )

    df = (
        pages.alias("wp")
        .join(qids.alias("q"), F.col("wp.qitem_id") == F.col("q.qitem_id"), "left")
        .select(
            "wp.wiki_db",
            "wp.page_id",
            "wp.qitem_id",
            "wp.page_title",
            "q.is_human",
            *content_gap,
        )
    )

    df = df.fillna(False, ["is_human"])

    prop.unpersist()
    pages.unpersist()
    qids.unpersist()

    return df


def main() -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--content_gaps",
        choices=gaps.all_gaps,
        nargs="+",
        default=gaps.all_gaps,
        help="Content gaps for which to generate features",
    )
    parser.add_argument(
        "--projects",
        type=lambda projects: projects.split(","),
        help="Comma separated list of projects.",
    )
    parser.add_argument(
        "--mediawiki_snapshot", help="Mediawiki snapshot, e.g. 2022-05.", required=True
    )
    parser.add_argument(
        "--wikidata_snapshot", help="Wikidata snapshot, e.g. 2022-05-23.", required=True
    )

    parser.add_argument(
        "--mode",
        choices=["production", "development"],
        default="production",
        help=(
            "Mode to run the script in. In production "
            "mode data will be generated using production "
            "tables, and in development mode smaller tables "
            "will be used to generate data."
        ),
    )

    parser.add_argument(
        "--dev_database_name",
        help="In development mode, this database used to retrieve smaller datasets.",
    )
    parser.add_argument(
        "--dev_table_prefix",
        default="",
        help="In development mode, the prefix can be used namespace tables.",
    )

    parser.add_argument(
        "--hdfs_dir",
        required=True,
        help="""The directory on hdfs that the pipeline uses to store
                        intermediate datasets""",
    )

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    content_features_df = get_content_features(
        spark,
        args.mediawiki_snapshot,
        args.wikidata_snapshot,
        args.projects,
        args.content_gaps,
        args.mode,
        args.dev_database_name,
        args.dev_table_prefix,
    )

    # for production as of August 2022, this generates ~40GB of data,
    # let's aim for ~100MB files
    file_partitions = 400

    if args.mode == "development":
        file_partitions = 10
        content_features_df = content_features_df.cache()
        content_features_df.show(n=50)

    (
        content_features_df.repartition(file_partitions)
        .write.mode("overwrite")
        .format("parquet")
        .save(files.content_gap_features(args.hdfs_dir))
    )


if __name__ == "__main__":
    main()
