import argparse
import os
from itertools import chain
from typing import cast

import pyspark.sql.functions as F
import pyspark.sql.types as T
from pyspark.sql import DataFrame, Row, SparkSession

from knowledge_gaps.config import files


def normalized(by_category: DataFrame, by_content_gap: DataFrame) -> DataFrame:
    """Returns a normalized content gap metrics dataframe.
    Schema:
        { wiki_db: { content_gap: { category : { metric: time_series }, totals : time_series }}}
    """  # noqa: E501

    @F.udf(returnType="struct<time_bucket:string,value:double>")
    def sparse_buckets(time_bucket: str, value: float) -> Row | None:
        if value is not None:
            return Row(time_bucket=time_bucket, value=float(value))
        else:
            return None

    # we only use the scalar metrics for the normalized dataset
    metric_field = next(
        field for field in by_category.schema if field.name == "metrics"
    )
    metric_schema = cast(T.StructType, metric_field.dataType)
    metric_cols = [field.name for field in metric_schema]

    # aggregate non-zero metric values into a sparse map time_bucket->value
    #  for each metric
    for c in metric_cols:
        by_category = by_category.withColumn(
            c, sparse_buckets("time_bucket", f"metrics.{c}")
        )
        by_content_gap = by_content_gap.withColumn(
            c, sparse_buckets("time_bucket", f"metrics.{c}")
        )

    by_category = by_category.drop("metrics", "quantiles")
    by_content_gap = by_content_gap.drop("metrics", "quantiles")

    # if the output is to be normalized, we collect the timeseris into arrays on a row,
    # and collect the categories into maps of arrays, so that the total is stored only
    # once per content gap.
    collect_sparse_timeseries = [
        F.map_from_entries(F.collect_list(c)).alias(c) for c in metric_cols
    ]
    by_category = by_category.groupby("wiki_db", "content_gap", "category").agg(
        *collect_sparse_timeseries
    )
    by_content_gap = by_content_gap.groupby("wiki_db", "content_gap").agg(
        *collect_sparse_timeseries
    )

    # transform the metric columns into a single metric map column
    metrics_map = F.create_map(
        list(chain(*((F.lit(k), F.col(k)) for k in metric_cols)))  # type: ignore
    )
    by_category = by_category.select(
        "wiki_db", "content_gap", "category", metrics_map.alias("metrics")
    )
    by_content_gap = by_content_gap.select(
        "wiki_db", "content_gap", metrics_map.alias("by_content_gap")
    )

    # collect the metrics map for each category into a map of map
    # { category: { metric: time_series }}
    @F.udf(returnType="struct<category:string,metrics:map<string, map<string,double>>>")
    def collect_categories(
        category: str, metrics: dict[str, dict[str, float]]
    ) -> Row | None:
        if metrics is not None:
            return Row(category=category, metrics=metrics)
        else:
            return None

    by_category = by_category.groupby("wiki_db", "content_gap").agg(
        F.map_from_entries(
            F.collect_list(collect_categories("category", "metrics"))
        ).alias("by_category")
    )

    sparse_df = by_category.join(by_content_gap, ["wiki_db", "content_gap"])
    return sparse_df


"""
Job to write output datasets after the knowledge gap pipeline has successfully ran.
"""


def main() -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--hdfs_dir",
        required=True,
        help="""The directory on hdfs that the pipeline uses to store
                        intermediate datasets""",
    )

    parser.add_argument(
        "--store_metric_features",
        action="store_true",
        help="If set, store the metric features on hive",
    )

    parser.add_argument(
        "--store_content_gap_features",
        action="store_true",
        help="If set, store the content gap features on hive",
    )

    parser.add_argument(
        "--store_normalized_metrics",
        action="store_true",
        help="""If set, store the normalized content gap metrics on hive
                    aggregates into a single json object
                    """,
    )

    parser.add_argument(
        "--store_csv_format",
        action="store_true",
        help="If set, store the csv format required by the knowledge gap index tool",
    )

    parser.add_argument("--database_name", help="The hive database to write outputs to")

    parser.add_argument("--table_prefix", default="", help="Optional prefix for tables")

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    # function to save a dataframe in the hive table specified in the args
    def save_to_hive(
        df: DataFrame,
        table_name: str,
        partition_keys: list[str],
        num_files: int | None = None,
    ) -> None:
        hive_table = f"{args.database_name}.{args.table_prefix}{table_name}"
        df = df.repartition(num_files) if num_files else df
        return (
            df.write.partitionBy(partition_keys)
            .mode("overwrite")
            .saveAsTable(hive_table)
        )

    # function to store a dataframe in a single parquet encoded file for publication
    def write_single_parquet(
        df: DataFrame, path: str, partition_keys: list[str] | None = None
    ) -> None:
        df_writer = df.repartition(1).write
        df_writer = (
            df_writer if not partition_keys else df_writer.partitionBy(partition_keys)
        )
        df_writer.mode("overwrite").parquet(path)

    # { wiki_db, content_gap, category, time_bucket, metrics }
    by_category = (
        spark.read.parquet(files.metrics_by_category(args.hdfs_dir))
        .orderBy("wiki_db", "content_gap", "category", "time_bucket")
        .cache()
    )
    save_to_hive(by_category, "by_category", ["time_bucket"], num_files=1)
    write_single_parquet(
        by_category,
        os.path.join(args.hdfs_dir, "single_parquet", "by_category"),
        partition_keys=["content_gap"],
    )

    # { wiki_db, content_gap, time_bucket, metrics }
    by_content_gap = (
        spark.read.parquet(files.metrics_by_content_gap(args.hdfs_dir))
        .orderBy("wiki_db", "content_gap", "time_bucket")
        .cache()
    )
    save_to_hive(by_content_gap, "by_content_gap", ["time_bucket"], num_files=1)
    write_single_parquet(
        by_content_gap,
        os.path.join(args.hdfs_dir, "single_parquet", "by_content_gap"),
    )

    # { content_gap, category, time_bucket, metrics }
    by_category_all_wikis = (
        spark.read.parquet(files.metrics_by_category_all_wikis(args.hdfs_dir))
        .orderBy("content_gap", "category", "time_bucket")
        .cache()
    )
    save_to_hive(
        by_category_all_wikis, "by_category_all_wikis", ["time_bucket"], num_files=1
    )
    write_single_parquet(
        by_category_all_wikis,
        os.path.join(args.hdfs_dir, "single_parquet", "by_category_all_wikis"),
    )

    # { content_gap, time_bucket, metrics }
    by_content_gap_all_wikis = (
        spark.read.parquet(files.metrics_by_content_gap_all_wikis(args.hdfs_dir))
        .orderBy("content_gap", "time_bucket")
        .cache()
    )
    save_to_hive(
        by_content_gap_all_wikis,
        "by_content_gap_all_wikis",
        ["time_bucket"],
        num_files=1,
    )
    write_single_parquet(
        by_content_gap_all_wikis,
        os.path.join(args.hdfs_dir, "single_parquet", "by_content_gap_all_wikis"),
    )

    if args.store_normalized_metrics:
        # store normalized version of content gap metrics
        normalized_df = normalized(by_category, by_content_gap)
        save_to_hive(normalized_df, "normalized", ["content_gap"], num_files=10)

    if args.store_content_gap_features:
        save_to_hive(
            spark.read.parquet(files.content_gap_features(args.hdfs_dir)),
            "content_gap_features",
            [],
        )

    if args.store_metric_features:
        save_to_hive(
            spark.read.parquet(files.metric_features(args.hdfs_dir)),
            "metric_features",
            [],
        )

    # the knowledge gap index tool requires a custom csv format
    if args.store_csv_format:
        # combine the by_category and by_content_gap metrics
        metrics_df = (
            by_category.alias("by_category")
            .join(
                by_content_gap.alias("by_content_gap"),
                on=["wiki_db", "time_bucket", "content_gap"],
            )
            .join(
                by_category_all_wikis.alias("by_category_all_wikis"),
                on=["time_bucket", "category", "content_gap"],
            )
        )

        wanted_cols = (
            [
                F.col("wiki_db"),
                F.col("content_gap"),
                F.col("category"),
                F.col("time_bucket"),
            ]
            + [
                F.col(f"by_category.metrics.{m}").alias(f"{m}_value")
                for m in metrics_df.select("by_category.metrics.*").columns
            ]
            + [
                F.col(f"by_content_gap.metrics.{m}").alias(f"{m}_all_categories")
                for m in metrics_df.select("by_content_gap.metrics.*").columns
            ]
            + [
                F.col(f"by_category_all_wikis.metrics.{m}").alias(f"{m}_all_wikis")
                for m in metrics_df.select("by_category_all_wikis.metrics.*").columns
            ]
        )

        csv_df = (
            metrics_df.select(*wanted_cols)
            .fillna(0)
            .orderBy("wiki_db", "content_gap", "category", "time_bucket")
            # this results in a single file per output partition
            # key, e.g. single file per content_gap
            .repartition(1)
        )

        output_path = files.make_hdfs_path(
            args.hdfs_dir, "knowledge_gap_index_metrics_csv"
        )
        (
            csv_df.write.option("header", True)
            .partitionBy("content_gap")
            .mode("overwrite")
            .csv(output_path, compression="none")
        )

        # generate a small sample used by accurat during development
        projects = ["enwiki", "swwiki", "arwiki", "bnwiki", "cawiki"]
        small_output_path = files.make_hdfs_path(
            args.hdfs_dir, "knowledge_gap_index_metrics_csv_small"
        )
        (
            csv_df.where(F.col("wiki_db").isin(projects))
            .write.option("header", True)
            .partitionBy("content_gap")
            .mode("overwrite")
            .csv(small_output_path, compression="none")
        )


if __name__ == "__main__":
    main()
