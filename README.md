# Knowledge Gaps

## Running

The knowledge gap pipeline consists of a sequence of steps, most of them spark jobs. An airflow DAG that executes these steps is [implemented in the airflow-dags repository](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/research/dags/knowledge_gaps_dag.py).

The invidiviual steps can be executed manually; e.g. when implementing a new content gap metric, the aggregation step will be repeatedly executed, either via spark-submit in a terminal or via a jupyter notebook.

## Develop running using airflow

The dag is in the [airflow-dags repository](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/research/dags/knowledge_gaps_dag.py).

The web UI of the research airflow instance is accessible at http://an-airflow1002.eqiad.wmnet:8600 (requires sshuttle or a ssh tunnel), to be used for production jobs.

For development, follow these steps:
- Clone [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)
- Create a directory where the `analytics-privatedata` user can create the airflow configuration directory, e.g. `sudo -u analytics-privatedata mkdir /tmp/research_test` (use a different name)
- Start the airflow dev instance using the `analytics-privatedata` user, with an unused port, and specifying the created config directory: `sudo -u analytics-privatedata ./run_dev_instance.sh  -m /tmp/research_test -p 8989 research`
- Set the airflow variable used to configure the knowledge gap pipeline via the airflow UI. In 'Admin/Variables' set the variable `knowledge_gaps_pipeline_config` to e.g. `{"mode": "development", "hdfs_temp_directory": "knowledge_gaps_dev", "output_database": "fab", "table_prefix": "output_", "start_time_bucket": "20220101"}`.

The dev instance will be accessible at the configured port, and uses the [research DAGs](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/research/dags) of the active branch of the airflow-dags repo.

Running the airflow instance as `analytics-privatedata` user is necessary as the airflow dags submit spark jobs using [skein] based(https://skeinyarn.com/) yarn containers, and for the kerberos credentials to properly propagate we need a user with a kerberos keytab (like `analytics-privatedata`) instead of a kerberos cache credential like the normal users.

## Running individual steps

The steps need to be run in sequential order as data generated in an earlier step is used in
a later step. Running steps individually is mostly useful for development, for example when implementing a new content gap metric, the "content gap features" and "feature metrics" steps can be executed once, followed by repeated executions of the "content gap metrics aggregation" step during development.

### Setup

Activate a conda environment, install the knowledge_gaps package, i.e. from the cloned directory run `pip install .`

Each step can run in one of two modes: `production` (to be used from airflow only) or `development`. In order to run in the development mode, we need to generate data first. See `./knowledge_gaps/config/development.sql` on how to generate development data.

```bash
spark3-sql \
    --master yarn \
    -f knowledge_gaps/config/development.sql \
    --num-executors 64 \
    --executor-cores 4 \
    --executor-memory 8G \
    --conf hive.exec.dynamic.partition.mode=nonstrict \
    -d mediawiki_snapshot=2023-06 \
    -d wikidata_snapshot=2023-06-26 \
    -d wikis="'abwiki'" \
    -d database_name=$(whoami) \
    -d table_prefix=dev_
```


The conda environment used by the spark workers running on yarn is defined as a env variable. This can be a locally built environment, or a http link pointing to a [conda env built by CI](https://gitlab.wikimedia.org/repos/research/knowledge-gaps/-/packages). Examples:
```
export CONDA_ENV=https://gitlab.wikimedia.org/api/v4/projects/212/packages/generic/knowledge-gaps/0.4.0/knowledge-gaps-0.4.0.conda.tgz
```
Using the [latest_package_file](https://gitlab.wikimedia.org/repos/research/research-common/-/blob/main/research_common/gitlab.py#L3) helper method.
```
export CONDA_ENV=$(python - <<EOF
from research_common.gitlab import latest_package_file
print(latest_package_file('repos/research/knowledge-gaps'))
EOF
)
```

### Article Quality Scores

The [article-quality](https://gitlab.wikimedia.org/repos/research/article-quality) repo contains a [standalone pipeline](https://gitlab.wikimedia.org/repos/research/article-quality/-/blob/main/setup.cfg#L20), executed via airflow dag.

The `development.sql` hive script populates a dev table of article quality scores from the production table. If you need to create a custom article quality scores dataset, follow the instructions of [article-quality](https://gitlab.wikimedia.org/repos/research/article-quality) to generate data needed for feature metrics (below).


### Pageviews

The `development.sql` hive script populates a dev table of daily pageviews from the production table.

The hourly pageview data on hive is preprocessed into daily aggregates.

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --conf spark.sql.shuffle.partitions=2000 \
    --conf spark.hadoop.hive.exec.dynamic.partition.mode=nonstrict \
    --conf spark.sql.legacy.timeParserPolicy=LEGACY \
    --archives $CONDA_ENV#env \
    knowledge_gaps/pageviews.py \
    --mode range \
    --start_date 20230601 \
    --end_date 20230602 \
    --database_name $(whoami) \
    --table_prefix dev_ \
    --overwrite true
```

### Feature Metrics

```bash
HADOOP_CONF_DIR=/etc/hadoop/conf \
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
    spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --archives $CONDA_ENV#env \
    knowledge_gaps/feature_metrics.py \
    20120101 20230101 \
    --time_bucket_freq monthly \
    --mediawiki_snapshot 2023-06 \
    --mode development \
    --dev_database_name $(whoami) \
    --dev_table_prefix dev_ \
    --hdfs_dir knowledge_gaps_dev
```

### Content gap features

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 8G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --archives $CONDA_ENV#env \
    knowledge_gaps/content_features.py \
    --content_gaps gender sexual_orientation geographic time \
    --mediawiki_snapshot 2023-06 \
    --wikidata_snapshot 2023-06-26 \
    --mode development \
    --dev_database_name $(whoami) \
    --dev_table_prefix dev_ \
    --hdfs_dir knowledge_gaps_dev
```

### Aggregation

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --archives $CONDA_ENV#env \
    knowledge_gaps/aggregation.py \
    --content_gaps gender \
    --hdfs_dir knowledge_gaps_dev
```

### Output datasets

```bash
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --archives $CONDA_ENV#env \
    knowledge_gaps/output_datasets.py \
    --store_content_gap_features \
    --store_normalized_metrics \
    --store_knowledge_gap_index_metrics \
    --database_name $(whoami) \
    --table_prefix output_ \
    --hdfs_dir knowledge_gaps_dev
```
## Publishing survey datasets

Besides the pipelines that process large datasets to compute the content gaps metrics, there are also survey based datasets. They are smaller in size and are collected and aggregated outside the data engineering infrastructure. The knowledge gap pipeline provides functionality to publish survey based datasets:
- a survey folder in the research hdfs directory that serves as authorative source for survey datasets for publication (`/wmf/data/research/knowledge_gaps/surveys`)
- a mechnanism to publish the survey datasets via airflow to the public [analyics web folder](https://analytics.wikimedia.org/published/datasets/knowledge_gaps/)

Follow these steps to publish aggregated survey data:

- Create a csv containing the desired survey data. Ensure that no PII data is included.
- The file name should include the gap it is associate with (e.g. `gender`,  `geography`)
- If data is available per project, there should a column `wiki_db` (example value `enwiki`). Otherwise, the file name should end with `_all_wikis.csv`
- If data is available per gap category, e.g. `woman` for the gender gap, the  column should be be called `category`
- From a [stat client](https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Clients), add the file to the relevant folder on hdfs. The main surveys folder (`/wmf/data/research/knowledge_gaps/surveys`) contains sub folders `readers`, `contributors`, `content`.  For example, assuming you created a file `gender_gap.csv` about readers on your local machine:
```
# copy the data to a stat client
$ scp gender_gaps.csv stat1008.eqiad.wmnet:
# add data to surveys folder on hdfs
$ ssh stat1008.eqiad.wmnet
stat1008:~$ hdfs dfs -put gender_gap.csv /wmf/data/research/knowledge_gaps/surveys/readers
```
- In case you don't have access to data engineering infrastructure, create a phabricator task for research engineering to assist.
- Upon the next scheduled run of the knowledge gaps pipeline via airflow, the dataset will be [published](https://analytics.wikimedia.org/published/datasets/knowledge_gaps/surveys).
- Add documentation for the released dataset to the results section on [meta](https://meta.wikimedia.org/wiki/Research:Knowledge_Gaps_Index/Measurement#Results) and [meta](https://meta.wikimedia.org/wiki/Research:Knowledge_Gaps_Index/Datasets)


## Development

### Prerequisites
- Create and activate the conda environment:
```bash
conda env create -f conda-environment.yaml
conda activate knowledge-gaps
```
- Install dependencies:
```bash
pip install .[dev,test]
```

### Testing
To run all tests:
```bash
python3 -m pytest -vv
```

### Typechecking
You can make sure your code typechecks using mypy:
```bash
python3 -m mypy
```

### Hooks
To set up the pre-commit hooks, run:
```bash
pre-commit install
```
This will lint and format the code on every commit. By default, it will only run on changed files, but you can run it on all files manually:
```bash
pre-commit run --all-files
```
Tests, typechecking and hooks are also run in the CI.

# Releasing

We don't currently have an established release model. That will come.

## Versioning

The `knowlede-gaps` module, and its conda distribution, is versioned according
to a [semver](https://semver.org/) scheme.

To help propagate version bumps across multiple file locations, a [bump2version](https://github.com/c4urself/bump2version) config is provided in `.bumpversion.cfg`.

For example, the following command
```
bump2version patch
```
will increase the patch version in `setup.cfg`.
Changes will need to be manually committed to git.
