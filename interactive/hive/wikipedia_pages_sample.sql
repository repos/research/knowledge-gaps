-- Creates a table and populates it with sample data that can be used as
-- an output of `wikipedia_pages_df()` from the file `page_history.py`.

-- How to run:
-- 1. Open a screen session: screen -S hive -L -Logfile ~/hive.log
-- 2. Run: hive -f wikipedia_pages_sample.sql

SET hive.mapred.mode = nonstrict;
SET hive.exec.dynamic.partition.mode = nonstrict;
SET hive.strict.checks.large.query = false;

USE bmansurov;
DROP TABLE wikipedia_pages_20220315;

CREATE TABLE wikipedia_pages_20220315 AS
SELECT p.wiki_db, p.page_id, p.page_title,
    wipl.item_id AS qitem_id,
FROM wmf_raw.mediawiki_page AS p
LEFT JOIN wmf.wikidata_item_page_link AS wipl
ON p.wiki_db = wipl.wiki_db
    AND p.page_id = wipl.page_id
WHERE p.snapshot='2021-11'
    AND p.page_namespace=0
    AND p.page_is_redirect=0
    AND p.wiki_db in ('enwiki', 'uzwiki')
    AND wipl.snapshot = '2021-12-13'
    AND wipl.page_namespace = 0
LIMIT 1000;
