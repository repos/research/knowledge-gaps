from knowledge_gaps.config import gap_categories, gap_properties, geography


def test_gap_categories() -> None:
    def test(categories: gap_categories.Categories) -> None:
        assert len(categories) > 0
        assert all(k.startswith("Q") for k, v in categories)

    test(gap_categories.load_gender_gap_categories())
    test(gap_categories.load_sexual_orientation_gap_categories())


def test_gap_properties() -> None:
    def test(properties: gap_properties.Properties) -> None:
        assert len(properties) > 0
        assert all(k.startswith("P") for k, v in properties)

    test(gap_properties.load_country_properties())
    test(gap_properties.load_time_properties())


def test_geography() -> None:
    qid_to_qid = geography.load_aggregation_logic()
    assert len(qid_to_qid) > 0
    assert all(k.startswith("Q") for k, v in qid_to_qid)
