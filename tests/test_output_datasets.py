import chispa
from pyspark import Row
from pyspark.sql import SparkSession

from knowledge_gaps import output_datasets


def test_normalize(spark: SparkSession) -> None:
    metrics = Row(
        article_created=1,
        pageviews_sum=1,
        pageviews_mean=1.0,
        standard_quality=1.0,
        standard_quality_count=1,
        quality_score=1.0,
        revision_count=1,
    )
    metrics_missing = Row(
        article_created=1,
        pageviews_sum=None,
        pageviews_mean=None,
        standard_quality=1.0,
        standard_quality_count=1,
        quality_score=1.0,
        revision_count=1,
    )

    schema = "struct<wiki_db:string,category:string,time_bucket:string,metrics:struct<article_created:bigint,pageviews_sum:bigint,pageviews_mean:double,standard_quality:double,standard_quality_count:bigint,quality_score:double,revision_count:bigint>,quantiles:struct<article_created_quantiles:array<int>,pageviews_quantiles:array<bigint>,quality_score_quantiles:array<float>,revision_count_quantiles:array<bigint>>,content_gap:string>"  # noqa: E501
    data = (
        ("fr", "xxx", "yyyy-MM", metrics, None, "gap"),
        ("fr", "xxx", "yyyy-MM1", metrics, None, "gap"),
        ("fr", "xxx", "yyyy-MM2", metrics_missing, None, "gap"),
        ("fr", "yyy", "yyyy-MM2", metrics, None, "gap"),
    )
    by_category = spark.createDataFrame(data, schema)

    schema = "struct<wiki_db:string,time_bucket:string,metrics:struct<article_created:bigint,pageviews_sum:bigint,pageviews_mean:double,standard_quality:double,standard_quality_count:bigint,quality_score:double,revision_count:bigint>,quantiles:struct<article_created_quantiles:array<int>,pageviews_quantiles:array<bigint>,quality_score_quantiles:array<float>,revision_count_quantiles:array<bigint>>,content_gap:string>"  # noqa: E501
    data = (
        ("fr", "yyyy-MM", metrics, None, "gap"),
        ("fr", "yyyy-MM1", metrics, None, "gap"),
        ("fr", "yyyy-MM2", metrics_missing, None, "gap"),
    )  # type: ignore[assignment]  # variable reuse
    by_content_gap = spark.createDataFrame(data, schema)

    normalized_df = output_datasets.normalized(by_category, by_content_gap)

    expected_data = (
        (
            {
                "xxx": {
                    "pageviews_mean": {"yyyy-MM": 1.0, "yyyy-MM1": 1.0},
                    "article_created": {
                        "yyyy-MM": 1.0,
                        "yyyy-MM2": 1.0,
                        "yyyy-MM1": 1.0,
                    },
                    "pageviews_sum": {"yyyy-MM": 1.0, "yyyy-MM1": 1.0},
                    "quality_score": {"yyyy-MM": 1.0, "yyyy-MM2": 1.0, "yyyy-MM1": 1.0},
                    "standard_quality_count": {
                        "yyyy-MM": 1.0,
                        "yyyy-MM2": 1.0,
                        "yyyy-MM1": 1.0,
                    },
                    "standard_quality": {
                        "yyyy-MM": 1.0,
                        "yyyy-MM2": 1.0,
                        "yyyy-MM1": 1.0,
                    },
                    "revision_count": {
                        "yyyy-MM": 1.0,
                        "yyyy-MM2": 1.0,
                        "yyyy-MM1": 1.0,
                    },
                },
                "yyy": {
                    "pageviews_mean": {"yyyy-MM2": 1.0},
                    "article_created": {"yyyy-MM2": 1.0},
                    "pageviews_sum": {"yyyy-MM2": 1.0},
                    "quality_score": {"yyyy-MM2": 1.0},
                    "standard_quality_count": {"yyyy-MM2": 1.0},
                    "standard_quality": {"yyyy-MM2": 1.0},
                    "revision_count": {"yyyy-MM2": 1.0},
                },
            },
        ),
    )
    expected_df = spark.createDataFrame(expected_data, ("by_category",))
    expected_df.schema["by_category"].nullable = False

    chispa.assert_df_equality(normalized_df.select("by_category"), expected_df)
