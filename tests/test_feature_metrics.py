import chispa
from pyspark.sql import SparkSession

from knowledge_gaps import feature_metrics


def test_filter_non_canonical_wikis(spark: SparkSession) -> None:
    canonical_wikis_data = [
        ("enwiki",),
        ("elwiki",),
        ("frwiki",),
        ("ruwiki",),
        ("eswiki",),
        ("plwiki",),
    ]

    canonical_wikis_df = spark.createDataFrame(canonical_wikis_data, ["database_code"])

    pageviews_data = [
        ("eswiki", "123412", "2023-06-11", "9", "2023-06"),
        ("enwiki", "09876", "2023-06-08", "13", "2023-06"),
        ("wikiquote", None, "2023-06-14", "342", "2023-06"),
    ]

    pageview_df = spark.createDataFrame(
        pageviews_data,
        ["wiki_db", "page_id", "time_bucket", "pageviews", "time_partition"],
    )

    expected_data = [
        ("eswiki", "123412", "2023-06-11", "9", "2023-06"),
        ("enwiki", "09876", "2023-06-08", "13", "2023-06"),
    ]

    expected_df = spark.createDataFrame(
        expected_data,
        ["wiki_db", "page_id", "time_bucket", "pageviews", "time_partition"],
    )

    filter_result_df = feature_metrics.filter_non_canonical_wikis(
        canonical_wikis_df=canonical_wikis_df, feature_metric_df=pageview_df
    )

    # chispa.assert_df_equality(filter_result_df.select("wiki_db"), expected_df)
    chispa.assert_df_equality(filter_result_df, expected_df)


def test_non_sparse_time_buckets(spark: SparkSession) -> None:
    article_created = [
        ("enwiki", 1, "2020-01", 1),
        ("enwiki", 2, "2020-03", 1),
    ]

    article_created_df = spark.createDataFrame(
        article_created, ["wiki_db", "page_id", "time_bucket", "article_created"]
    )

    time_buckets = ["2020-01", "2020-02", "2020-03", "2020-04"]
    expected = [
        ("enwiki", 1, "2020-01"),
        ("enwiki", 1, "2020-02"),
        ("enwiki", 1, "2020-03"),
        ("enwiki", 1, "2020-04"),
        ("enwiki", 2, "2020-03"),
        ("enwiki", 2, "2020-04"),
    ]

    chispa.assert_df_equality(
        feature_metrics.get_all_timebuckets(article_created_df, time_buckets),
        spark.createDataFrame(expected, ["wiki_db", "page_id", "time_bucket"]),
    )

    time_buckets = ["2020-02", "2020-03", "2020-04"]
    expected = [
        ("enwiki", 1, "2020-02"),
        ("enwiki", 1, "2020-03"),
        ("enwiki", 1, "2020-04"),
        ("enwiki", 2, "2020-03"),
        ("enwiki", 2, "2020-04"),
    ]

    chispa.assert_df_equality(
        feature_metrics.get_all_timebuckets(article_created_df, time_buckets),
        spark.createDataFrame(expected, ["wiki_db", "page_id", "time_bucket"]),
    )

    time_buckets = ["2020-01", "2020-02"]
    expected = [
        ("enwiki", 1, "2020-01"),
        ("enwiki", 1, "2020-02"),
    ]

    chispa.assert_df_equality(
        feature_metrics.get_all_timebuckets(article_created_df, time_buckets),
        spark.createDataFrame(expected, ["wiki_db", "page_id", "time_bucket"]),
    )
