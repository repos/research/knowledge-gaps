import chispa
from pyspark.sql import SparkSession

from knowledge_gaps import aggregation


def test_aggregate_knowledge_gap_metrics(spark: SparkSession) -> None:
    cols = (
        "my_category",
        "wiki_db",
        "time_bucket",
        "article_created",
        "pageviews",
        "standard_quality",
        "quality_score",
        "page_revision_count",
    )

    data = (
        ("xxx", "fr", "yyyy-MM", 0, 100, 0, 0.25, 2),
        ("xxx", "fr", "yyyy-MM", 2, 300, 1, 0.75, 3),
        ("yyy", "fr", "yyyy-MM", 2, 500, 1, 0.8, 4),
    )

    df = spark.createDataFrame(data, cols)

    aggs = aggregation.aggregate_knowledge_gap_metrics(df, "my_category")

    # metrics_by_category

    metrics_by_category = aggs["metrics_by_category"]

    out_df = metrics_by_category.select(["category", "metrics.*"])

    out_cols = (
        "category",
        "article_created",
        "pageviews_sum",
        "pageviews_mean",
        "standard_quality",
        "standard_quality_count",
        "quality_score",
        "revision_count",
    )

    expected_data = (
        ("xxx", 2, 400, 200.0, 0.5, 1, 0.5, 5),
        ("yyy", 2, 500, 500.0, 1.0, 1, 0.8, 4),
    )
    expected_df = spark.createDataFrame(expected_data, out_cols)

    chispa.assert_df_equality(out_df, expected_df, ignore_row_order=True)

    # metrics_by_category

    metrics_by_category = aggs["metrics_by_content_gap"]

    out_df = metrics_by_category.select(["metrics.*"])

    out_cols = (
        "article_created",
        "pageviews_sum",
        "pageviews_mean",
        "standard_quality",
        "standard_quality_count",
        "quality_score",
        "revision_count",
    )  # type: ignore[assignment]  # variable reuse

    expected_data = (
        (4, 900, 300.0, 2 / 3, 2, 0.6, 9),
    )  # type: ignore[assignment]  # variable reuse
    expected_df = spark.createDataFrame(expected_data, out_cols)

    chispa.assert_df_equality(out_df, expected_df, ignore_row_order=True)
