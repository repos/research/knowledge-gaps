# based on https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/lib/conda.yml

FROM docker-registry.wikimedia.org/wikimedia-buster:20210523

ARG wmf_workflow_utils=v0.5.1

RUN echo "deb http://apt.wikimedia.org/wikimedia buster-wikimedia thirdparty/conda" >> /etc/apt/sources.list.d/wikimedia.list
RUN apt update
RUN apt install -y curl gcc g++ gpg git make ca-certificates conda
RUN apt install -y libkrb5-dev libsasl2-dev
SHELL ["/bin/bash", "-c"]
RUN source /opt/conda/etc/profile.d/conda.sh && \
    conda activate  && \
    pip install tox==3.24.4 && \
    pip install git+https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils.git@${wmf_workflow_utils}
